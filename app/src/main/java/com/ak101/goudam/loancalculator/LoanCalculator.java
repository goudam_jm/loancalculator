package com.ak101.goudam.loancalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;


public class LoanCalculator extends AppCompatActivity {

    private final int[] YEARS = {5, 10, 15, 20, 25, 30, 35};
    private int numOfYears = YEARS.length;
    private double savedLoanAmount;
    private double savedLoanRate;
    private final String LOAN_AMOUNT = "LOAN_AMOUNT";
    private final String LOAN_RATE = "LOAN_RATE";
    private double[] emi = new double[numOfYears];
    private double[] loanTotal = new double[numOfYears];
    private EditText loanAmount;
    private EditText rate;

    private EditText[] loanTotalEditText = new EditText[numOfYears];
    private EditText[] loanEMIEditText = new EditText[numOfYears];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_calculator);
        if(savedInstanceState != null){
            savedLoanAmount = 0.00;
            savedLoanRate = 0.00;
        }
        else{
            savedLoanAmount = savedInstanceState.getDouble(LOAN_AMOUNT);
            savedLoanRate = savedInstanceState.getDouble(LOAN_RATE);
        }
        loanAmount = (EditText) findViewById(R.id.loanEditText);
        rate = (EditText) findViewById(R.id.rateEditText);

        loanTotalEditText[0] = (EditText) findViewById(R.id.fiveLoanTotalEditText);
        loanEMIEditText[0] = (EditText) findViewById(R.id.fiveLoanEMIEditText);
        loanTotalEditText[1] = (EditText) findViewById(R.id.tenLoanTotalEditText);
        loanEMIEditText[1] = (EditText) findViewById(R.id.tenLoanEMIEditText);
        loanTotalEditText[2] = (EditText) findViewById(R.id.fifteenLoanTotalEditText);
        loanEMIEditText[2] = (EditText) findViewById(R.id.fifteenLoanEMIEditText);
        loanTotalEditText[3] = (EditText) findViewById(R.id.twentyLoanTotalEditText);
        loanEMIEditText[3] = (EditText) findViewById(R.id.twentyLoanEMIEditText);
        loanTotalEditText[4] = (EditText) findViewById(R.id.twentyfiveLoanTotalEditText);
        loanEMIEditText[4] = (EditText) findViewById(R.id.twentyfiveLoanEMIEditText);
        loanTotalEditText[5] = (EditText) findViewById(R.id.thirtyLoanTotalEditText);
        loanEMIEditText[5] = (EditText) findViewById(R.id.thirtyLoanEMIEditText);
        loanTotalEditText[6] = (EditText) findViewById(R.id.thirtyfiveLoanTotalEditText);
        loanEMIEditText[6] = (EditText) findViewById(R.id.thirtyfiveLoanEMIEditText);

        loanAmount.addTextChangedListener(textWatcher);
        rate.addTextChangedListener(textWatcher);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_loan_calculator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putDouble(LOAN_AMOUNT,savedLoanAmount);
        outState.putDouble(LOAN_RATE,savedLoanRate);
    }

    /*
        TextWatcher listener for EditText of Loan amount and loan rate
     */
    private TextWatcher textWatcher  = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length() == 0){}
            else{
                if(Double.parseDouble(rate.getText().toString()) > 0){
                    calculateLoanAndEMI();
                    updateLoanUI();
                }
            }
        }
    };

    /*
        Function for calculating total loan and emi for a period of 35 years
     */
    private void calculateLoanAndEMI(){
        int n;

        double numerator;
        double denominator;

        double P;
        double r;

        P = Double.parseDouble(loanAmount.getText().toString()) ;
        r = Double.parseDouble(rate.getText().toString()) ;

        r = r/1200;

        for(int i=0; i<numOfYears; ++i){
            n = YEARS[i] * 12;
            numerator = P * r * Math.pow((1+ r),n);
            denominator = Math.pow((1+r), n) - 1;
            emi[i] = numerator/denominator;
            loanTotal[i] = emi[i] * n;
        }
    }

    /*
        Function for updating the UI with calculated loan and emi amounts
     */
    private void updateLoanUI(){
        for(int i=0; i<numOfYears; ++i){
            loanTotalEditText[i].setText(""+loanTotal[i]);
            loanEMIEditText[i].setText(""+emi[i]);
            loanTotalEditText[i].setText(String.format("%.02f",loanTotal[i]));
            loanEMIEditText[i].setText(String.format("%.02f",emi[i]));
        }

    }

}
